import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthPassService } from 'src/app/service/auth-pass.service';
import { Router } from '@angular/router';
import { ViewApplicationService } from 'src/app/service/view-application.service';

@Component({
  selector: 'app-bankbranch',
  templateUrl: './bankbranch.component.html',
  styleUrls: ['./bankbranch.component.css']
})
export class BankbranchComponent implements OnInit {

  constructor(private http: HttpClient,
    private rejectedLoans: AuthPassService,
    private viewApplication: ViewApplicationService,
    private router: Router) {

  }
  datatables: any = []
  selectedValue: any = []
  common_IP: any;
  status: any
  roid: any
  bankid: any
  unitid: any
  solid: any
  latitude: any
  longitude: any
  state: any
  branchname: any
  employeename:any

  ngOnInit() {
    this.common_IP = JSON.parse(sessionStorage.getItem('commonIP'))
    this.getData(event)
  }
  searchText;


  p: number = 1;
  public pageSize: number = 5;
  datatablesData = [];
  customerid: any
  checkedItems = []


  getevent(event) {
    $(document).ready(function () {
      let strings = $('#selectall').click(function () {
      });
      console.log(strings)
      let string = $('.option').prop("checked", true);
    });
  }
  getData(event) {
    const base_URL = 'http://localhost:9007/getBankBranchList'
    this.http.get(base_URL, {
    }).subscribe((data) => {
      console.log(base_URL)
      console.log(data)
      this.datatables.push(data)
      this.datatables = this.datatables[0]
    })
  }

  createnew(event) {
    $('#addNew_with_modal').modal('toggle');
    const base_URL = 'http://localhost:9007/saveBankBranchData'
   
    this.status = (<HTMLInputElement>document.getElementById("status")).value
    this.state = (<HTMLInputElement>document.getElementById("state")).value
    this.branchname = (<HTMLInputElement>document.getElementById("branchname")).value
    this.employeename = (<HTMLInputElement>document.getElementById("employeename")).value

    this.http.post(base_URL, {

   

      employeename:this.employeename,
      branchname:this.branchname,
      state: this.state,
      status: this.status,
    

    }).subscribe(data => {
      console.log(this.longitude)
      console.log(this.latitude)
      console.log(this.solid)
      console.log(this.unitid)
      console.log(this.bankid)
      console.log(this.roid)
      console.log(this.status)

  
        alert("Data Created Successfully")
        window.location.reload(true);

    })
  }
}
